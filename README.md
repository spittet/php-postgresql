# PHP and PostgreSQL for Bitbucket Pipelines

This repository contains a Dockerfile as well as a simple example that shows how you can run your own Docker container with PHP and PostgreSQL on Bitbucket Pipelines.

The Docker image is using PHP 7.1.1 and PostgreSQL 9.4.

## Quickstart

### Using the image with Bitbucket Pipelines

Just copy/paste the YML below in your bitbucket-pipelines.yml and adapt the script to your needs.

```yaml
# This is a sample build configuration for PHP.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Dockerhub as your build environment.
image: spittet/php-postgresql

pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - /etc/init.d/postgresql start
          - sudo -u postgres sh -c 'createdb phptest'
          - sudo -u postgres sh -c "psql -c \"CREATE USER myuser WITH PASSWORD 'mypassword';\""
          - php test.php
```

### Using this in a script

You'll find a sample script in this repository in test.rb. It simply connects to PostgreSQL and then lists the existing databases.

```php
<?php

$conn = new PDO("pgsql:host=127.0.0.1;dbname=phptest;user=myuser;password=mypassword;");

$sql = 'SELECT datname FROM pg_database WHERE datistemplate = false;';

$q = $conn->query($sql);

while ($row = $q->fetch()){
  print_r($row);
}

```

## Create your own image

If you want to use a different version of PHP you can simply create your own image for it. Just copy the content of the Dockerfile and replace the first line.

This image is built from the official PHP image at https://hub.docker.com/_/php/ and you can find there all the different versions that are supported.

Your Dockerfile won't need to have an ENTRYPOINT or CMD line as Bitbucket Pipelines will run the script commands that you put in your bitbucket-pipelines.yml file instead.

```
FROM php:7.1.1
RUN apt-get update \
  && apt-get install -y postgresql postgresql-contrib \
  && apt-get install sudo \
  && apt-get install -y libpq-dev \
  && docker-php-ext-install pdo pdo_pgsql \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# This Dockerfile doesn't need to have an entrypoint and a command
# as Bitbucket Pipelines will overwrite it with a bash script.
```

### Build the image

```bash
docker build -t <your-docker-account>/php-postgresql .
```

### Run the image locally with bash to make some tests

```bash
docker run -i -t <your-docker-account>/php-postgresql /bin/bash
```

### Push the image back to the Docker Hub

```bash
docker push <your-docker-account>/php-postgresql
```
